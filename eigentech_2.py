# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

class ReluActivator(object):
    def forward(self, input):
        return input*(input>0)

    def backward(self, output):
        return 1.0 * (output>0)

class FullConnectedLayer():
    def __init__(self, input_size, output_size, activator):
        self.input_size = input_size  #输入维度
        self.output_size = output_size  #输出维度
        self.activator = activator  #激活函数
        self.w = np.random.uniform(0, 0.1, (output_size, input_size))  #权值矩阵self.w
        self.b = np.zeros((output_size, 1))  #偏置self.b
        self.w_grad_total = np.zeros((output_size, input_size))
        self.b_grad_total = np.zeros((output_size, 1))

    def forward(self, input_data):
        self.input_data = input_data
        self.output_data = self.activator.forward(np.dot(self.w, self.input_data) + self.b)

    def backward(self, input_delta):
        # input_delta_为后一层传入的误差
        # output_delta为传入前一层的误差
        self.sen = input_delta * self.activator.backward(self.output_data)
        output_delta = np.dot(self.w.T, self.sen)
        self.w_grad = np.dot(self.sen, self.input_data.T)
        self.b_grad = self.sen
        self.w_grad_total += self.w_grad
        self.b_grad_total += self.b_grad
        return output_delta

    def update(self, lr):  #随机梯度下降法
        self.w -= lr * self.w_grad
        self.b -= lr * self.b_grad

class Network():
    def __init__(self, params_array, activator):
        # params_array为层维度信息超参数数组，layers为神经网络的层集合
        self.layers = []
        for i in range(len(params_array) - 1):
            self.layers.append(FullConnectedLayer(params_array[i], params_array[i + 1], activator))

    # 网络前向迭代
    def predict(self, sample):
        # 下面一行的output可以理解为输入层输出
        output = sample
        for layer in self.layers:
            layer.forward(output)
            output = layer.output_data
        return output

    # 网络反向迭代
    def calc_gradient(self, label):
        delta = (self.layers[-1].output_data - label)
        for layer in self.layers[::-1]:
            delta = layer.backward(delta)
        return delta

    # 一次训练一个样本 ，然后更新权值
    def train_one_sample(self, sample, label, lr):
        self.predict(sample)
        Loss = self.loss(self.layers[-1].output_data, label)
        self.calc_gradient(label)
        self.update(lr)
        return Loss

    # 一次训练一批样本 ，然后更新权值
    def train_batch_sample(self, sample_set, label_set, lr, batch):
        Loss = 0.0
        for i in range(batch):
            self.predict(sample_set[i])
            Loss += self.loss(self.layers[-1].output, label_set[i])
            self.calc_gradient(label_set[i])
        self.update(lr)
        return Loss

    def update(self, lr):
        for layer in self.layers:
            layer.update(lr)

    def loss(self, pred, label):
        return 0.5 * ((pred - label) * (pred - label)).sum()


if __name__ == '__main__':
    params = [100, 10, 100]
    activator = ReluActivator()
    net = Network(params, activator)
    data = np.random.randn(100, 1)
    label = np.random.randint(0,2,(100,1))
    for i in range(100):
        loss = net.train_one_sample(data, label, 2)
    print '准确率: '
    predict = net.predict(data)
    print np.mean(predict == label)

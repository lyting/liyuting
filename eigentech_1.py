# -*- coding: utf-8 -*-
'''
Numbers = [123, 895, 119, 1037]
Matrix =[
[1, 2, 3, 4],
[3, 5, 9, 8],
[8, 0, 3, 7],
[6, 1, 9, 2]
]
'''
#输入
Numbers = [int(i) for i in raw_input().slit()]
#需给出矩阵的行数M与列数N
Matrix = []
for i in range(M):
    Matrix.append([int(j) for j in raw_input().slit()])

#查找矩阵的某一列
def index_lie(Matrix,j):
    lie = []
    for i in range(len(Matrix)):
        lie.append(Matrix[i][j])
    return lie
#查找某个数是否在矩阵中，若在矩阵中，得到具体所在位置
def chazhao_k(Matrix,a):
    for k in range(len(Matrix)):
        GPS = []
        if a[0] in Matrix[k]:
            GPS.append([k,Matrix[k].index(a[0])])
            for i in range(1,len(a)):
                if a[i] in Matrix[GPS[-1][0]]:
                    if abs(Matrix[GPS[-1][0]].index(a[i])-GPS[-1][1])==1:
                        GPS.append([GPS[-1][0], Matrix[GPS[-1][0]].index(a[i])])
                elif a[i] in index_lie(Matrix,GPS[-1][1]):
                    if abs(index_lie(Matrix, GPS[-1][1]).index(a[i])-GPS[-1][0]) == 1:
                        GPS.append([index_lie(Matrix, GPS[-1][1]).index(a[i]), GPS[-1][1]])
                else:
                    break
            if len(GPS) == len(a):
                break
    return GPS

ret = []
for i in range(len(Numbers)):
    a = list(str(Numbers[i]))
    a = [int(j) for j in a]
    GPS = chazhao_k(Matrix,a)  #得到矩阵中包含数字的具体位置
    if len(GPS) == len(a):
        ret.append(Numbers[i])
        for i,j in GPS:   #同一个单元格内的数字不允许被重复使用
            Matrix[i][j] = 'NA'
#输出结果
print(ret)





